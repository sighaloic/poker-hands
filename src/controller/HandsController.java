package controller;

import model.Card;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class HandsController {

    public HandsController() {
    }

    Scanner sc = new Scanner(System.in);
    List<Card> cards = new ArrayList<>();

    public String royalFlush(List<Card> hand){
        String result = "False";

//        Get the first color to compare with orthers
        String color = hand.get(0).getColor();

//       create own Royal Flush
        cards.add(new Card(color,"A"));
        cards.add(new Card(color,"K"));
        cards.add(new Card(color,"Q"));
        cards.add(new Card(color,"J"));
        cards.add(new Card(color,"10"));

        int fRComp = 0;

//        check if the hand is a Royal Flush
        List<String> r = new ArrayList<>();
        for (Card c : cards) {
            for (Card h : hand) {
                if(c.getColor().equals(h.getColor()) && c.getRank().equals(h.getRank()))
                    fRComp++;
                if(! r.contains(h.getRank()+""+h.getColor()))
                r.add(h.getRank()+""+h.getColor());
            }
        }
        if(fRComp == 5){
            result = " Royal Flush : True -> " + r;
        }
        return result;
    }

    public String straightFlush(List<Card> hand){
        String result = "False";


//        Get the first color to compare with orthers
        String color = hand.get(0).getColor();
//        temporal cards list which use to sort cards and compare with the schema
        List<String> tmpCards = new ArrayList<>();

//       create a schema of hierarchy
        cards.add(new Card(color,"K"));
        cards.add(new Card(color,"Q"));
        cards.add(new Card(color,"J"));
        cards.add(new Card(color,"10"));
        cards.add(new Card(color,"9"));
        cards.add(new Card(color,"8"));
        cards.add(new Card(color,"7"));
        cards.add(new Card(color,"6"));
        cards.add(new Card(color,"5"));
        cards.add(new Card(color,"4"));
        cards.add(new Card(color,"3"));
        cards.add(new Card(color,"2"));
        cards.add(new Card(color,"A"));

        int fRComp = 0;

//        check if the hand is a Royal Flush
        List<String> r = new ArrayList<>();
        for (Card c : cards) {
            for (Card h : hand) {
                if(c.getColor().equals(h.getColor()) && c.getRank().equals(h.getRank()))
                    tmpCards.add(h.getRank() + "" + h.getColor());
            }
        }
        if(tmpCards.size() == 5){
            result = " Straight Flush : True -> " + tmpCards;
        }
        return result;
    }

    public String straight(List<Card> hand){
        String result = "False";


//        Get the first color to compare with orthers
        String color = hand.get(0).getColor();
//        temporal cards list which use to sort cards and compare with the schema
        List<Card> tmpCards = new ArrayList<>();
        List<Card> tmpCards0 = new ArrayList<>();
        List<String> tmpC = new ArrayList<>();

//       create a schema of hierarchy
        cards.add(new Card(color,"K"));
        cards.add(new Card(color,"Q"));
        cards.add(new Card(color,"J"));
        cards.add(new Card(color,"10"));
        cards.add(new Card(color,"9"));
        cards.add(new Card(color,"8"));
        cards.add(new Card(color,"7"));
        cards.add(new Card(color,"6"));
        cards.add(new Card(color,"5"));
        cards.add(new Card(color,"4"));
        cards.add(new Card(color,"3"));
        cards.add(new Card(color,"2"));
        cards.add(new Card(color,"A"));

//        check if the hand is a Straight
        int tmp = 0;
        int tmp0 = 0;
        int i = 0;
        List<String> r = new ArrayList<>();
        for (Card c : cards) {
            for (Card h : hand) {
                if(c.getRank().equals(h.getRank())) {
                    tmp++;
                    if(! tmpCards.contains(h))
                        tmpCards.add(h);
                }
            }
        }

        if(tmpCards.size() == 5){
            tmp = 0;
            for (Card c1 : tmpCards) {
                for (Card c2 : tmpCards) {
                    if(c1.getColor().equals(c2.getColor())) {
                        tmp++;
                    }
                }

                if(tmp == 1 || tmp == 2) {
                    tmp0++;
                    tmpCards0.add(c1);
                    tmp = 0;
                }
            }

            if(tmp0 == 5) {
                tmp = 0;
                tmp0 = 0;
                for (Card c1 : tmpCards0)
                    for (Card c2 : tmpCards0)
                        if (c1.getRank().equals(c2.getRank())) {
                            tmp++;
                            tmpC.add(c1.getRank()+""+c1.getColor());
                        }
                if (tmp == 5)
                    result = " Straight : True -> " + tmpC;
            }
        }
        return result;
    }

    public String fourOfKind(List<Card> hand){
        String result = "False";

//        check if the hand is a Four of a kind
        int tmp1 = 0;
        List<String> r = new ArrayList<>();
        for (Card c1 : hand) {
            for (Card c2 : hand) {
                if (c1.getRank().equals(c2.getRank())) {
                    tmp1++;
                    r.add(c1.getRank()+""+c2.getColor());
                }
            }
            if(tmp1 == 4) {
                result = " Four Of Kind : True -> rang : " + r;
            }

            tmp1 = 0;
            r.clear();

        }

        return result;
    }

    public String twoPair(List<Card> hand){
        String result = "False";

//        check if the hand is a Two Pair (two sets of a pair)
        int tmp1 = 0;
        List<String> r = new ArrayList<>();
        for (Card c1 : hand) {
            for (Card c2 : hand) {
                if (c1.getRank().equals(c2.getRank())) {
                    tmp1++;
//                    r.add(c1.getRang());
                }
            }
            if (tmp1 == 2)
                r.add(c1.getRank()+""+c1.getColor());
//            System.out.println("Pairs : " + r + ", " + tmp1);
            tmp1 = 0;
//            r.clear();
        }
//        System.out.println("size of pair : " + r.size());
        if(r.size() == 4)
            result = " Two Pair : True -> " + r;
        return result;
    }

    public String pair(List<Card> hand){
        String result = "False";

//        check if the hand is a Pair (only one set of a pair)
        int tmp1 = 0;
        int tmp2 = 0;
        int tmp3 = 0;
        List<String> r = new ArrayList<>();
        for (Card c1 : hand) {
            for (Card c2 : hand) {
                if (c1.getRank().equals(c2.getRank())) {
                    tmp1++;
//                    r.add(c1.getRang());
                }
            }
            if (tmp1 == 2) {
                tmp2 = tmp1;
                r.add(c1.getRank() + "" + c1.getColor());
            }
            if (tmp1 == 3) {
                tmp3 = tmp1;
                r.add(c1.getRank() + "" + c1.getColor());
            }
//            System.out.println("Pairs : " + r + ", tmp2:" + tmp2 + ", tmp3:" + tmp3 );
            tmp1 = 0;
//            r.clear();
        }
//        System.out.println("size of pair : " + r.size());
        if(r.size() == 2 && tmp3 == 0)
            result = " Pair : True -> " + r;
        return result;
    }

    public String full(List<Card> hand){
        String result = "False";

//        check if the hand is a Full (ONe set of three same rang and one set of a pair)
        int tmp1 = 0;
        int tmp2 = 0;
        int tmp3 = 0;
        List<String> r = new ArrayList<>();
        for (Card c1 : hand) {
            for (Card c2 : hand) {
                if (c1.getRank().equals(c2.getRank())) {
                    tmp1++;
//                    r.add(c1.getRang());
                }
            }
            if (tmp1 == 2) {
                tmp2 = tmp1;
                r.add(c1.getRank() + "" + c1.getColor());
            }
            if (tmp1 == 3) {
                tmp3 = tmp1;
                r.add(c1.getRank() + "" + c1.getColor());
            }
//            System.out.println("Pairs : " + r + ", tmp2:" + tmp2 + ", tmp3:" + tmp3 );
            tmp1 = 0;
//            r.clear();
        }
//        System.out.println("size of full : " + r.size());
        if(r.size() == 5 && tmp3 == 3)
            result = " Full : True -> " + r;
        return result;
    }

    public String flush(List<Card> hand){
        String result = "False";

//        check if the hand is a Flush (hand of same color)
        int tmp = 0;
        int tmp0 = 0;
        List<String> r = new ArrayList<>();
        for (Card c1 : hand) {
            for (Card c2 : hand) {
                if (c1.getColor().equals(c2.getColor())) {
                    tmp++;
                }
            }
            if (tmp == 5) {
                tmp0 = tmp;
                r.add(c1.getRank() + "" + c1.getColor());
            }

//            System.out.println("Pairs : " + r + ", tmp2:" + tmp2 + ", tmp3:" + tmp3 );
            tmp = 0;
//            r.clear();
        }
//        System.out.println("size of full : " + r.size());
        if(r.size() == 5 && tmp0 == 5)
            result = " Flush : True -> " + r;
        return result;
    }

    public String threeOfKind(List<Card> hand){
        String result = "False";

//        check if the hand is a Three Of Kind (ONe set of three same rang and others different)
        int tmp1 = 0;
        int tmp2 = 0;
        int tmp3 = 0;
        List<String> r = new ArrayList<>();
        for (Card c1 : hand) {
            for (Card c2 : hand) {
                if (c1.getRank().equals(c2.getRank())) {
                    tmp1++;
//                    r.add(c1.getRang());
                }
            }
            if (tmp1 == 3) {
                tmp2 = tmp1;
                r.add(c1.getRank() + "" + c1.getColor());
            }
            if (tmp1 == 2) {
                tmp3 = tmp1;
                r.add(c1.getRank() + "" + c1.getColor());
            }
//            System.out.println("Pairs : " + r + ", tmp2:" + tmp2 + ", tmp3:" + tmp3 );
            tmp1 = 0;
//            r.clear();
        }
//        System.out.println("size of full : " + r.size());
        if(r.size() == 3 && tmp3 == 0)
            result = " Three Of Kind : True -> " + r;
        return result;
    }

    public String highCard(List<Card> hand){
        String result = "False";

//        check if the hand is a High Card (hand of all cards different)
        int tmp = 0;
        int tmp0 = 0;
        List<String> r = new ArrayList<>();
        for (Card c1 : hand) {
            for (Card c2 : hand) {
                if (c1.getRank().equals(c2.getRank())) {
                    tmp++; // for every rang, this var must be equals to 1
                }
                if (c1.getColor().equals(c2.getColor())) {
                    tmp0++; // for every rang, this var must be equals to 1
                }
            }
            if (tmp == 1 && tmp0 < 3) {
                tmp = 0;
                tmp0 = 0;
                r.add(c1.getRank() + "" + c1.getColor());
            }

        }

        if(r.size() == 5) // we must have a list of exactly five different cards
            result = " High Card : True -> " + r;
        return result;
    }

}
