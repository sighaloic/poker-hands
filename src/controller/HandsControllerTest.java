package controller;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class HandsControllerTest {

    @BeforeEach
    void setUp() {
    }

    @Test
    void royalFlush() {
    }

    @Test
    void straightFlush() {
    }

    @Test
    void straight() {
    }

    @Test
    void fourOfKind() {
    }

    @Test
    void twoPair() {
    }

    @Test
    void pair() {
    }

    @Test
    void full() {
    }

    @Test
    void flush() {
    }

    @Test
    void threeOfKind() {
    }

    @Test
    void highCard() {
    }
}