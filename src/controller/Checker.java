package controller;

import model.Card;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Checker {

    public Checker() {
    }

    HandsController hc = new HandsController();

    public void checker(){

        Scanner sc = new Scanner(System.in);
        String rank, color;
        Card c1 = new Card();Card c2 = new Card();Card c3 = new Card();Card c4 = new Card();Card c5 = new Card();
        List<Card> hand = new ArrayList<>();
        List<String> h = new ArrayList<>();

//        input of cards by user
//        ######################################################################################

        System.out.println("Let's start with the card N°1");
        System.out.print("Enter the rank : ");
        rank = sc.nextLine();
        c1.setRank(rank.toUpperCase());

        System.out.print("Enter the color : ");
        color = sc.nextLine();
        c1.setColor(color.toUpperCase());

        hand.add(c1);
        System.out.println(hand);

//        ---------------------------------------------------

        System.out.println("Let's start with the card N°2");
        System.out.print("Enter the rank : ");
        rank = sc.nextLine();
        c2.setRank(rank.toUpperCase());

        System.out.print("Enter the color : ");
        color = sc.nextLine();
        c2.setColor(color.toUpperCase());

        int tmp = 0;
        for(Card c : hand){
            if(c.getRank().equals(c2.getRank()) && c.getColor().equals(c2.getColor()))
                tmp++;
        }

        if(tmp != 0){
            while (tmp != 0){

                System.out.println();

                System.out.println("\t\t !!! Sorry you already got this card, try whit another !!!");
                System.out.println("Let's start with the card N°2");
                System.out.print("Enter the rank : ");
                rank = sc.nextLine();
                c2.setRank(rank.toUpperCase());

                System.out.print("Enter the color : ");
                color = sc.nextLine();
                c2.setColor(color.toUpperCase());

                tmp = 0;
                for(Card c : hand){
                    if(c.getRank().equals(c2.getRank()) && c.getColor().equals(c2.getColor()))
                        tmp++;
                }
                System.out.println("tmp : " + tmp);
            }
            hand.add(c2);
        }else
            hand.add(c2);

//        ---------------------------------------------------

        System.out.println("Let's start with the card N°3");
        System.out.print("Enter the rank : ");
        rank = sc.nextLine();
        c3.setRank(rank.toUpperCase());

        System.out.print("Enter the color : ");
        color = sc.nextLine();
        c3.setColor(color.toUpperCase());

        tmp = 0;
        for(Card c : hand){
            if(c.getRank().equals(c3.getRank()) && c.getColor().equals(c3.getColor()))
                tmp++;
        }

        if(tmp != 0){
            while (tmp != 0){

                System.out.println();

                System.out.println("\t\t !!! Sorry you already got this card, try whit another !!!");
                System.out.println("Let's start with the card N°3");
                System.out.print("Enter the rank : ");
                rank = sc.nextLine();
                c3.setRank(rank.toUpperCase());

                System.out.print("Enter the color : ");
                color = sc.nextLine();
                c3.setColor(color.toUpperCase());

                tmp = 0;
                for(Card c : hand){
                    if(c.getRank().equals(c3.getRank()) && c.getColor().equals(c3.getColor()))
                        tmp++;
                }
                System.out.println("tmp : " + tmp);
            }
            hand.add(c3);
        }else
            hand.add(c3);

//        ---------------------------------------------------

        System.out.println("Let's start with the card N°4");
        System.out.print("Enter the rank : ");
        rank = sc.nextLine();
        c4.setRank(rank.toUpperCase());

        System.out.print("Enter the color : ");
        color = sc.nextLine();
        c4.setColor(color.toUpperCase());

        tmp = 0;
        for(Card c : hand){
            if(c.getRank().equals(c4.getRank()) && c.getColor().equals(c4.getColor()))
                tmp++;
        }

        if(tmp != 0){
            while (tmp != 0){

                System.out.println();

                System.out.println("\t\t !!! Sorry you already got this card, try whit another !!!");
                System.out.println("Let's start with the card N°4");
                System.out.print("Enter the rank : ");
                rank = sc.nextLine();
                c4.setRank(rank.toUpperCase());

                System.out.print("Enter the color : ");
                color = sc.nextLine();
                c4.setColor(color.toUpperCase());

                tmp = 0;
                for(Card c : hand){
                    if(c.getRank().equals(c4.getRank()) && c.getColor().equals(c4.getColor()))
                        tmp++;
                }
                System.out.println("tmp : " + tmp);
            }
            hand.add(c4);
        }else
            hand.add(c4);

//        ---------------------------------------------------

        System.out.println("Let's start with the card N°5");
        System.out.print("Enter the rank : ");
        rank = sc.nextLine();
        c5.setRank(rank.toUpperCase());

        System.out.print("Enter the color : ");
        color = sc.nextLine();
        c5.setColor(color.toUpperCase());

        tmp = 0;
        for(Card c : hand){
            if(c.getRank().equals(c5.getRank()) && c.getColor().equals(c5.getColor()))
                tmp++;
        }

        if(tmp != 0){
            while (tmp != 0){

                System.out.println();

                System.out.println("\t\t !!! Sorry you already got this card, try whit another !!!");
                System.out.println("Let's start with the card N°5");
                System.out.print("Enter the rank : ");
                rank = sc.nextLine();
                c5.setRank(rank.toUpperCase());

                System.out.print("Enter the color : ");
                color = sc.nextLine();
                c5.setColor(color.toUpperCase());

                tmp = 0;
                for(Card c : hand){
                    if(c.getRank().equals(c5.getRank()) && c.getColor().equals(c5.getColor()))
                        tmp++;
                }
                System.out.println("tmp : " + tmp);
            }
            hand.add(c5);
        }else
            hand.add(c5);

//        ######################################################################################


        System.out.println();
        System.out.println();
        System.out.println();
        System.out.println();

//        Parse from Object list to String list in order to display cards
        for (Card c : hand) {
            h.add(c.getRank()+""+c.getColor());
        }
        System.out.println("Your cards are " + h);
        System.out.println("and your possible hand(s) is(are) :");

        System.out.println();

        String result = null;

        result = hc.royalFlush(hand);
        if(!result.equals("False")) {
            System.out.println("****************************** 1. Royal Flush ***********"); // Done
            System.out.println(result);
            System.out.println();
        }

        result = hc.straightFlush(hand);
        if(!result.equals("False")) {
            System.out.println("****************************** 2. Straight Flush ***********"); // Done
            System.out.println(result);
            System.out.println();
        }

        result = hc.fourOfKind(hand);
        if(!result.equals("False")) {
            System.out.println("****************************** 3. Four Of Kind/Quads ***********"); // Done
            System.out.println(result);
            System.out.println();
        }

        result = hc.full(hand);
        if(!result.equals("False")) {
            System.out.println("****************************** 4. Full ***********"); // Done
            System.out.println(result);
            System.out.println();
        }

        result = hc.flush(hand);
        if(!result.equals("False")) {
            System.out.println("****************************** 5. Flush ***********"); // Done
            System.out.println(result);
            System.out.println();
        }

        result = hc.straight(hand);
        if(!result.equals("False")) {
            System.out.println("****************************** 6. Straight ***********"); // Done
            System.out.println(result);
            System.out.println();
        }

        result = hc.threeOfKind(hand);
        if(!result.equals("False")) {
            System.out.println("****************************** 7. Three Of Kind/Trips/Set ***********"); // Done
            System.out.println(result);
            System.out.println();
        }

        result = hc.twoPair(hand);
        if(!result.equals("False")) {
            System.out.println("****************************** 8. Two Pair ***********"); // Done
            System.out.println(result);
            System.out.println();
        }

        result = hc.pair(hand);
        if(!result.equals("False")) {
            System.out.println("****************************** 9. One Pair ***********"); // Done
            System.out.println(result);
            System.out.println();
        }

        result = hc.highCard(hand);
        if(!result.equals("False")) {
            System.out.println("****************************** 10. High Card ***********"); // Done
            System.out.println(result);
            System.out.println();
        }

    }
}
