package model;

public class Card {

    private String rank;
    private String color;

    public Card() {
    }

    public Card(String color, String rank) {
        this.rank = rank;
        this.color = color;
    }

    public String getRank() {
        return rank;
    }

    public void setRank(String rank) {
        this.rank = rank;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    @Override
    public String toString() {
        return "Card{" +
                "rang=" + rank +
                ", color='" + color + '\'' +
                '}';
    }


}
